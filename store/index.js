import axios from 'axios'
import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    state: {
      no: null,
      recipes: null
    },
    mutations: {
      setNo(state, data) {
        state.no = data
      },
      setRecipes(state, data) {
        state.recipes = data
      }
    },
    actions: {
      setNo({commit}, data) {
        commit('setNo', data)
      },
      setRecipes({commit}, data) {
        return new Promise((resolve, reject) => {
          axios.get('https://api.edamam.com/search?q='+ data + '&app_id=87dc6b39&app_key=21b0439f73d40762540d12bb2dcccc9d&from=0&to=100')
            .then(res => {
              console.log('response from api:', res)
              if(res.status == 200) {
                commit('setRecipes', res)
                resolve(true);
              } else {
                reject(false)
              }
              
            })
            .catch(error => {
               reject(false);
            }) 
        })
      }
    },
    getters: {
      getNo(state) {
        return state.no
      }
    }
  })
}
export default createStore